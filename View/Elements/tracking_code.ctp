<?php
if (empty($webPropertyId)) {
	return false;
}
// TODO: implement custom vars and events tracking
$gaCustomVarTemplate = "  _gaq.push(['_setCustomVar', %s, '%s', '%s', %s]);";
$gaEventTemplate = "  _gaq.push(['_trackEvent', '%s', '%s', '%s']);";
$events = '';
$customVars = '';
$trackingCode = <<<EOS
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '$webPropertyId', 'auto');
  ga('send', 'pageview');
EOS;
$trackingCode = $this->Html->scriptBlock($trackingCode);
echo $trackingCode;
